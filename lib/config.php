<?php

namespace Project\Idea;

class Config {

    const IBLOCK_CATEGORIES = 113;
    const BLOG_ID = 140;
    const COLLECTION_TIME = 14;
    const SECTIONS = 534;
    const COLLECTION_USER = array(3, 27, 710);
    const STATUS_VOTE_ID = 114;
    const STATUS_SOGLASOVANO_ID = 113;
    const STATUS_SOGLASOVANO = 'SOGLASOVANO';
    const STATUS_PROCESSING_ID = 114;
    const STATUS_PROCESSING = 'PROCESSING';
    const POST_URL = '/idea/#post_id#/';
    const VOTE_TIME_PROCESSING = '1 months';
    const VOTE_SETTING = array(
        'TITLE' => 'Как вам идея?',
        'ANSWERS' => array(
            'Мне нравится!',
            'Не нравится'
        )
    );
    const VOTE_STATUS = array(
        'CANCEL' => 116,
        'COMPLETED' => 115,
    );
    const VOTE_RESULT = array(
        'Otklonen' => 117,
        'Ne_progolosovali' => 118,
        'Progolosovali' => 119,
        'Na_golosovanii' => 120,
    );

}
