<?

namespace Project\Idea\Helper;

use CBlogPost,
    CSocNetLog,
    Tm\Portal;

class Post {

    static public function addVote($arFields, $arVote) {
        global $UF_BLOG_POST_VOTE_n0_DATA;
        $UF_BLOG_POST_VOTE_n0_DATA = array(
            'ID' => '',
            'URL' => '',
            'QUESTIONS' => array(
                array(
                    'QUESTION' => $arVote['TITLE'],
                    'QUESTION_TYPE' => 'text',
                    'ID' => '',
                    'ANSWERS' => array()
                )
            )
        );
        foreach ($arVote['ANSWERS'] as $value) {
            $UF_BLOG_POST_VOTE_n0_DATA['QUESTIONS'][0]['ANSWERS'][] = array(
                'MESSAGE' => $value,
                'MESSAGE_TYPE' => 'text',
                'FIELD_TYPE' => '0',
                'ID' => '',
            );
        }
//        preClear();
//        pre($UF_BLOG_POST_VOTE_n0_DATA);
        $UF_BLOG_POST_FILE = $arFields['UF_BLOG_POST_FILE'];
        unset($arFields['UF_BLOG_POST_FILE']);
        $postID = CBlogPost::Add($arFields);
        global $USER_FIELD_MANAGER;
        $USER_FIELD_MANAGER->Update('BLOG_POST', $postID, array(
            'UF_BLOG_POST_FILE' => $UF_BLOG_POST_FILE
        ));

//        preError();
////        pre($postID, $arFields);

        $arFieldsHave = array(
            "HAS_IMAGES" => 'N',
            "HAS_TAGS" => 'N',
            "HAS_PROPS" => 'Y',
            "HAS_SOCNET_ALL" => 'Y',
        );
        CBlogPost::Update($postID, $arFieldsHave, false);

        $arFields["ID"] = $postID;
        $logId = self::socialnetworkLog($arFields);
        CSocNetLog::Update(intval($logId), array(
            "EVENT_ID" => \Bitrix\Blog\Integration\Socialnetwork\Log::EVENT_ID_POST_VOTE
        ));
//        preExit($logId);
        return $postID;
    }

    static public function socialnetworkLog($arFields) {
        return Portal\Helper\Post::socialnetworkLog($arFields, '/idea/?page=post&post_id=#post_id#');
    }

}
