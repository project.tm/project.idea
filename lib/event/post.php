<?

namespace Project\Idea\Event;

use CBlog,
    CBlogPost,
    CBlogTools,
    CBlogUser,
    CSocNetLog,
    Bitrix\Main\Loader,
    Bitrix\Blog\PostSocnetRightsTable,
    Project\Idea\Config,
    Tm\Portal,
    Project\Idea\Helper,
    Project\Tools\Trains;

class Post {

    use Trains\Event;

    static protected function evetType() {
        return __CLASS__;
    }

    public static function OnBeforePostUpdate($ID, &$arFields) {
        if (self::start()) {
            if (isset($arFields['UF_STATUS']) and empty($arFields['UF_IDEA_RESULT']) and $arFields['UF_STATUS'] == Config::VOTE_STATUS['CANCEL']) {
                $arFields['UF_IDEA_RESULT'] = Config::VOTE_RESULT['Otklonen'];
            }
            self::stop();
        }
    }

    public static function OnPostUpdate($ID, &$arFields) {
        if (self::start()) {
            if (isset($arFields['UF_STATUS']) and $arFields['UF_STATUS'] == Config::STATUS_VOTE_ID) {
                $arPost = CBlogPost::GetById($ID);
                if ($arPost['BLOG_ID'] == Config::BLOG_ID) {
                    $arPost['AUTHOR_ID'] = 1;
                    $eventId = \Bitrix\Blog\Integration\Socialnetwork\Log::EVENT_ID_POST_VOTE;
                    $arVote = array(
                        'TITLE' => 'Новая идея для голосования',
                        'DETAIL_TEXT' => implode(PHP_EOL, array(
                            $arPost['TITLE'],
                            $arPost['DETAIL_TEXT']
                        )),
                        'DETAIL_TEXT_TYPE' => 'text',
                        'DATE_PUBLISH' => date('d.m.Y H:i:s'),
                        'PUBLISH_STATUS' => 'P',
                        'CATEGORY_ID' => '',
                        'PATH' => '/company/personal/user/' . $arPost['AUTHOR_ID'] . '/blog/#post_id#/',
                        'URL' => 'u' . $arPost['AUTHOR_ID'] . '-blog-s1',
                        'PERMS_POST' => array(),
                        'PERMS_COMMENT' => array(),
//                    'MICRO' => 'Y',
                        'SOCNET_RIGHTS' => array(
                            0 => 'UA',
                        ),
                        'UF_BLOG_POST_IMPRTNT' => '0',
                        'UF_BLOG_POST_VOTE' => 'n0',
                        'UF_BLOG_POST_FILE' => array(
//                        'n95028'
                        ),
                        'UF_BLOG_POST_URL_PRV' => '',
                        'SEARCH_GROUP_ID' => '1',
                        'AUTHOR_ID' => $arPost['AUTHOR_ID'],
                        'BLOG_ID' => Portal\Helper\Blog::getByUserId($arPost['AUTHOR_ID']),
                        'PREVIEW_TEXT_TYPE' => 'text',
                        'ENABLE_TRACKBACK' => 'Y',
                        'ENABLE_COMMENTS' => 'Y',
                        'SC_PERM' => array(
                            0 => 'G2',
                        ),
                    );
//                preClear();

                    global $USER_FIELD_MANAGER;
                    $arPostFields = $USER_FIELD_MANAGER->GetUserFields("BLOG_POST", $ID, LANGUAGE_ID);
                    if (!empty($arPostFields['UF_BLOG_POST_FILE']['VALUE'])) {
                        $arVote['UF_BLOG_POST_FILE'] = $arPostFields['UF_BLOG_POST_FILE']['VALUE'];
                    }

                    $postID = Helper\Post::addVote($arVote, Config::VOTE_SETTING);
                    global $USER_FIELD_MANAGER;
                    $USER_FIELD_MANAGER->Update('BLOG_POST', $ID, array(
                        'UF_POST_VOTE' => $postID,
                        'UF_IDEA_RESULT' => Config::VOTE_RESULT['Na_golosovanii']
                    ));
                }
            }
            self::stop();
        }
    }

    public static function OnBeforePostAdd(&$arFields) {
//        return;
//        if (self::start() and isset($arFields['BLOG_ID']) and $arFields['BLOG_ID'] == Config::BLOG_ID) {
////            $arFields['SC_PERM'][] = 'U' . Portal\Helper\Deportament::getHeadByUser($arFields['AUTHOR_ID']);
//            $arFields['SC_PERM'][] = 'UA';
//            $arFields['SOCNET_RIGHTS'][] = 'U' . Portal\Helper\Deportament::getHeadByUser($arFields['AUTHOR_ID']);
//            self::stop();
//        }
    }

    public static function OnPostAdd($postID, &$arFields) {
//        preExport($arFields);
//        return;
        if (self::start()) {
            if (isset($arFields['BLOG_ID']) and $arFields['BLOG_ID'] == Config::BLOG_ID) {
                $arFields["ID"] = $postID;
                Helper\Post::socialnetworkLog($arFields);
            }
            self::stop();
        }
    }

}
