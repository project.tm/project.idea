<?

define('BITRIX24_INDEX_PAGE', true);
define('BITRIX_INDEX_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetAdditionalCSS('/local/templates/markup/dist/idea.css');
$APPLICATION->AddHeadScript('/local/templates/markup/dist/idea.js');
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetPageProperty("title", htmlspecialcharsbx(COption::GetOptionString("main", "site_name", "Bitrix24")));
?>


<?

if (SITE_TEMPLATE_ID !== "bitrix24")
  return;
?>
<?
$arPopupParams = array(
    'ACTION' => 'edit',
    'TYPE' => 'header',
    'FORM_PARAMETERS' =>
    array (
        'ID' => rand(999, 9999999),
        'GROUP_ID' => '',
        'USER_ID' => cUser::GetID(),
        'PATH_TO_USER_TASKS' => '/company/personal/user/#user_id#/tasks/',
        'PATH_TO_USER_TASKS_TASK' => '/company/personal/user/#user_id#/tasks/task/#action#/#task_id#/',
        'PATH_TO_GROUP_TASKS' => '/workgroups/group/#group_id#/tasks/',
        'PATH_TO_GROUP_TASKS_TASK' => '',
        'PATH_TO_USER_PROFILE' => '/company/personal/user/#user_id#/',
        'PATH_TO_GROUP' => '/workgroups/group/#group_id#/',
        'PATH_TO_USER_TASKS_PROJECTS_OVERVIEW' => '/company/personal/user/#user_id#/tasks/projects/',
        'PATH_TO_USER_TASKS_TEMPLATES' => '/company/personal/user/#user_id#/tasks/templates/',
        'PATH_TO_USER_TEMPLATES_TEMPLATE' => '/company/personal/user/#user_id#/tasks/templates/template/#action#/#template_id#/',
        'SET_NAVCHAIN' => 'Y',
        'SET_TITLE' => 'Y',
        'SHOW_RATING' => 'Y',
        'RATING_TYPE' => 'like',
        'NAME_TEMPLATE' => '#LAST_NAME# #NAME#',
    )
);
$APPLICATION->IncludeComponent("project.idea:portal.iframe.popup", "wrap", $arPopupParams);
?>



<?/*$APPLICATION->IncludeComponent(
  "bitrix:idea.popup",
  "",
  Array(
    "AUTH_TEMPLATE" => "",
    "BLOG_URL" => "idea_s1",
    "BUTTON_COLOR" => "#b9ec2c",
    "CACHE_TIME" => "3600",
    "CACHE_TYPE" => "A",
    "CATEGORIES_CNT" => "4",
    "COMPOSITE_FRAME_MODE" => "A",
    "COMPOSITE_FRAME_TYPE" => "AUTO",
    "DISABLE_EMAIL" => "Y",
    "DISABLE_SONET_LOG" => "Y",
    "FORGOT_PASSWORD_URL" => "",
    "IBLOCK_CATEGORIES" => "113",
    "LIST_MESSAGE_COUNT" => "8",
    "PATH_IDEA_INDEX" => "/services/idea/",
    "PATH_IDEA_POST" => "/services/idea/#post_id#/",
    "POST_BIND_STATUS_DEFAULT" => "112",
    "RATING_TEMPLATE" => "standart",
    "REGISTER_URL" => "",
    "SHOW_RATING" => "Y"
  )
);*/?>

<?$APPLICATION->IncludeComponent(
  "bitrix:idea",
  "",
  Array(
    "ALLOW_POST_CODE" => "Y",
    "BLOG_URL" => "idea_s1",
    "CACHE_TIME" => "3600",
    "CACHE_TIME_LONG" => "604800",
    "CACHE_TYPE" => "A",
    "COMMENTS_COUNT" => "25",
    "COMMENT_ALLOW_VIDEO" => "N",
    "COMMENT_EDITOR_DEFAULT_HEIGHT" => "200",
    "COMPOSITE_FRAME_MODE" => "A",
    "COMPOSITE_FRAME_TYPE" => "AUTO",
    "DATE_TIME_FORMAT" => "d.m.Y H:i:s",
    "DISABLE_EMAIL" => "Y",
    "DISABLE_RSS" => "N",
    "DISABLE_SONET_LOG" => "Y",
    "EDITOR_CODE_DEFAULT" => "N",
    "EDITOR_DEFAULT_HEIGHT" => "300",
    "EDITOR_RESIZABLE" => "Y",
    "IBLOCK_CATEGORIES" => "113",
    "IBLOCK_CATOGORIES" => "113",
    "IMAGE_MAX_WIDTH" => "800",
    "MESSAGE_COUNT" => "25",
    "NAME_TEMPLATE" => "#NOBR##LAST_NAME# #NAME##/NOBR#",
    "NAV_TEMPLATE" => "",
    "NO_URL_IN_COMMENTS" => "",
    "PATH_TO_SMILE" => "/bitrix/images/blog/smile/",
    "POST_BIND_STATUS_DEFAULT" => "112",
    "POST_BIND_USER" => array("1"),
    "RATING_TEMPLATE" => "like",
    "SEF_MODE" => "N",
    "SET_NAV_CHAIN" => "Y",
    "SET_TITLE" => "Y",
    "SHOW_LOGIN" => "Y",
    "SHOW_RATING" => "Y",
    "TAGS_COUNT" => "0",
    "USE_ASC_PAGING" => "N",
    "USE_GOOGLE_CODE" => "Y",
    "VARIABLE_ALIASES" => Array(
      "category" => "category",
      "category_1" => "category_1",
      "category_2" => "category_2",
      "page" => "page",
      "post_id" => "post_id",
      "status_code" => "status_code",
      "user_id" => "user_id"
    )
  )
);?>
<?
$arPopupParams['TYPE'] = 'footer';
$APPLICATION->IncludeComponent("project.idea:portal.iframe.popup", "wrap", $arPopupParams);
?>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>