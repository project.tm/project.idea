<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Project\Tools\Modules,
    Project\Tools\Utility;

Loader::includeModule('tm.portal');
IncludeModuleLangFile(__FILE__);

class project_idea extends CModule {

    public $MODULE_ID = 'project.idea';

    use Modules\Install;

    function __construct() {
        $this->setParam(__DIR__, 'PROJECT_IDEA');
        $this->MODULE_NAME = Loc::getMessage('PROJECT_IDEA_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_IDEA_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_IDEA_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('PROJECT_IDEA_PARTNER_URI');
    }

    /*
     * InstallEvent
     */

    public function InstallEvent() {
        $this->registerEventHandler('blog', 'OnBeforePostAdd', '\Project\Idea\Event\Post', 'OnBeforePostAdd');
        $this->registerEventHandler('blog', 'OnPostAdd', '\Project\Idea\Event\Post', 'OnPostAdd');
        $this->registerEventHandler('blog', 'OnBeforePostUpdate', '\Project\Idea\Event\Post', 'OnBeforePostUpdate');
        $this->registerEventHandler('blog', 'OnPostUpdate', '\Project\Idea\Event\Post', 'OnPostUpdate');
    }

    public function UnInstallEvent() {
        $this->unRegisterEventHandler('blog', 'OnBeforePostAdd', '\Project\Idea\Event\Post', 'OnBeforePostAdd');
        $this->unRegisterEventHandler('blog', 'OnPostAdd', '\Project\Idea\Event\Post', 'OnPostAdd');
        $this->unRegisterEventHandler('blog', 'OnBeforePostUpdate', '\Project\Idea\Event\Post', 'OnBeforePostUpdate');
        $this->unRegisterEventHandler('blog', 'OnPostUpdate', '\Project\Idea\Event\Post', 'OnPostUpdate');
    }

    /*
     * InstallAgent
     */

    public function InstallAgent() {
        Utility\Agent::add(
                '\Project\Idea\Agent\Calendar::process();', // имя функции
                $this->MODULE_ID, // идентификатор модуля
                'Y', // агент не критичен к кол-ву запусков
                60 * 60, // интервал запуска - 1 сутки
                '', // дата первой проверки - текущее
                'Y', // агент активен
                date('d.m.Y 23:59:59'), // дата первого запуска - текущее
                30);
        Utility\Agent::add(
                '\Project\Idea\Agent\Vote::process();', // имя функции
                $this->MODULE_ID, // идентификатор модуля
                'Y', // агент не критичен к кол-ву запусков
                24 * 60 * 60, // интервал запуска - 1 сутки
                '', // дата первой проверки - текущее
                'Y', // агент активен
                date('d.m.Y H:i:s'), // дата первого запуска - текущее
                30); 
    }

    public function UnInstallAgent() {
        CAgent::RemoveAgent('\Project\Idea\Agent\Calendar::process();', $this->MODULE_ID);
        CAgent::RemoveAgent('\Project\Idea\Agent\Vote::process();', $this->MODULE_ID);
    }

    /*
     * InstallFiles
     */

    public function InstallFiles($arParams = array()) {
//        CopyDirFiles($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/components/', $_SERVER['DOCUMENT_ROOT'] . '/local/components/', true, true);
    }

    public function UnInstallFiles() {
//        DeleteDirFiles($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/components/', $_SERVER['DOCUMENT_ROOT'] . '/local/components/');
    }

}
